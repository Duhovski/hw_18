#include <iostream>

using namespace std;

class Stack
{
	
private:
	int size; 
	int* array;
	int lastIndex = -1;

public:
	//���������� ������ ��� �������
	Stack(int sizeNew)
	{
		size = sizeNew;
		array = new int[size];
	}	

	//�������� ��������� ������� �������
	int pop()
	{
		if (lastIndex > 0)
		{			
			array[lastIndex] = 0;
			lastIndex--;
			cout << array[lastIndex] << endl;
		}
		else
		{
			cout << "!Stack is empty!" << endl;
		}

		return array[lastIndex];
	}
	
	//������ ��������
	void push(int value)
	{	
		//�������� �� ������������ �����
		if (lastIndex + 1 < size)
		{
			lastIndex++;
			array[lastIndex] = value;
			cout << array[lastIndex] << endl;
		}
		else
		{
			cout << "!Stack is over!" << endl;
		}
	}

	//�������� �� ��� � �������
	void print()
	{
		for (int i = 0; i < size; i++)
		{
			cout << array[i] << endl;
		}
	}

	~Stack()
	{
		delete[] array;
	}
};

int main()
{
	Stack stack(5);
	
	stack.push(1);
	
	stack.push(2);
	
	stack.push(3);	
	
	stack.push(4);
	
	stack.push(5);
	
	stack.push(6);

	stack.pop();
	stack.pop();
	stack.pop();
	stack.pop();
	stack.pop();
	
	
}